package Zadania;

public class Zad2 
{

	 public boolean isCorrectDate(int year, int month, int day) 
	 {
		 boolean correctDate = false;
         if (year >= 2001 && year <= 2099) 
         {
        	 
             switch (month) 
             {
             case 1, 3, 5, 7, 8, 10, 12:
             {
                    if (day <= 31 && day > 0) 
                    {
                        correctDate = true;
                        break;
                    }
                }
                case 2: 
                {
                    boolean february = ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);

                    if (day <= 28 && day > 0 && !february) 
                    {
                        correctDate = true;
                        break;
                    }
                    if (day <= 29 && day > 0 && february) 
                    {
                        correctDate = true;
                        break;
                    }
                    break;
                }
                case 4, 6, 9, 11: 
                {
                    if (day <= 30 && day > 0) 
                    {
                        correctDate = true;
                        break;
                    }
                }
                default: 
                {
                    correctDate = false;
                }
            }
        }

        return correctDate;
    }
}
